<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('image')->nullable();
            $table->boolean('is_extractable')->default(false);
            $table->boolean('buy_from_marketplace')->default(false);
            $table->integer('cost_from_marketplace')->default(0);
            $table->boolean('sell_to_marketplace')->default(false);
            $table->integer('cost_to_marketplace')->default(0);
            $table->integer('sort')->default(0);
            $table->boolean('live')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
