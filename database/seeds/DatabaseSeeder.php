<?php

use Illuminate\Database\Seeder;
use App\Models\Process;
use App\Models\Resource;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $iron = Resource::create([
            'name' => 'Iron',
            'image' => 'iron.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 1
        ]);
        $titanium = Resource::create([
            'name' => 'Titanium',
            'image' => 'titanium.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 2
        ]);
        $aluminium = Resource::create([
            'name' => 'Aluminium',
            'image' => 'aluminium.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 3
        ]);
        $carbon = Resource::create([
            'name' => 'Carbon',
            'image' => 'carbon.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 4
        ]);
        $copper = Resource::create([
            'name' => 'Copper',
            'image' => 'copper.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 5
        ]);
        $hydrogen = Resource::create([
            'name' => 'Hydrogen',
            'image' => 'hydrogen.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 2,
            'sort' => 6
        ]);
        $deuterium = Resource::create([
            'name' => 'Deuterium',
            'image' => 'deuterium.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 2,
            'sort' => 7
        ]);
        $methane = Resource::create([
            'name' => 'Methane',
            'image' => 'methane.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 2,
            'sort' => 8
        ]);
        $helium = Resource::create([
            'name' => 'Helium',
            'image' => 'helium.png',
            'is_extractable' => 1,
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 10,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 2,
            'sort' => 9
        ]);

        $hydrogenMethane = Resource::create([
            'name' => 'Hydrogen Methane',
            'image' => 'hydrogen-methane.png',
            'buy_from_marketplace' => 0,
            'cost_from_marketplace' => 0,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 1,
            'sort' => 11
        ]);

        $power = Resource::create([
            'name' => 'Power',
            'image' => 'power.png',
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 50,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 5
        ]);


        $fuelConvertor = Process::create([
            'name' => 'Fuel Convertor',
            'image' => 'fuel-converter.png',
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 20,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 2
        ]);
        $fuelConvertor->resources()->attach($hydrogenMethane->id, ['qty' => 1]);
        $fuelConvertor->resources()->attach($hydrogen->id, ['qty' => -1]);
        $fuelConvertor->resources()->attach($methane->id, ['qty' => -1]);

        $powerGenerator = Process::create([
            'name' => 'Power Generator',
            'image' => 'power-generator.png',
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 75,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 7
        ]);
        $powerGenerator->resources()->attach($power->id, ['qty' => 1]);
        $powerGenerator->resources()->attach($hydrogenMethane->id, ['qty' => -2]);

        $cokeOven = Process::create([
            'name' => 'Coke Oven',
            'image' => 'coke-oven.png',
            'buy_from_marketplace' => 1,
            'cost_from_marketplace' => 95,
            'sell_to_marketplace' => 1,
            'cost_to_marketplace' => 9
        ]);
        $cokeOven->resources()->attach($coke->id, ['qty' => 3]);
        $cokeOven->resources()->attach($carbon->id, ['qty' => -4]);
    }
}
