
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue'), name: 'home' },
      { path: 'resources', component: () => import('pages/Resources.vue'), name: 'resources' },
      { path: 'processes', component: () => import('pages/Processes.vue'), name: 'processes' },
      { path: 'stores', component: () => import('pages/Stores.vue'), name: 'stores' },
      { path: 'infrastructure', component: () => import('pages/Infrastructure.vue'), name: 'infrastructure' },
      { path: 'map', component: () => import('pages/Map.vue'), name: 'map' }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
