export function setSeconds (state, seconds) {
  state.seconds = seconds
}

export function incrementSeconds (state) {
  state.seconds = state.seconds + 1
}

export function setMarketResources (state, marketResources) {
  state.marketResources = marketResources
}

export function setMarketProcesses (state, marketProcesses) {
  state.marketProcesses = marketProcesses
}
