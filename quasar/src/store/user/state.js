export default {
  username: null,
  xp: null,
  level: null,
  beer: null,
  busyResources: [],
  idleResources: [],
  stocks: [],
  busyProcesses: [],
  idleProcesses: []
}
