export function setUsername (state, username) {
  state.username = username
}

export function setXp (state, xp) {
  state.xp = xp
}

export function setLevel (state, level) {
  state.level = level
}

export function setBeer (state, beer) {
  state.beer = beer
}

export function setBusy (state, data) {
  state['busy' + data.model.charAt(0).toUpperCase() + data.model.slice(1)] = data.collection
}

export function setIdle (state, data) {
  state['idle' + data.model.charAt(0).toUpperCase() + data.model.slice(1)] = data.collection
}

export function setStocks (state, stocks) {
  state.stocks = stocks
}

export function makeIdle (state, data) {
  state['idle' + data.model.charAt(0).toUpperCase() + data.model.slice(1)].push(state['busy' + data.model.charAt(0).toUpperCase() + data.model.slice(1)][data.index])
  state['busy' + data.model.charAt(0).toUpperCase() + data.model.slice(1)].splice(data.index, 1)
}

export function makeBusy (state, data) {
  state['busy' + data.model.charAt(0).toUpperCase() + data.model.slice(1)].push(state['idle' + data.model.charAt(0).toUpperCase() + data.model.slice(1)][data.index])
  state['idle' + data.model.charAt(0).toUpperCase() + data.model.slice(1)].splice(data.index, 1)
}
