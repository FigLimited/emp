import { LocalStorage } from 'quasar'
import axios from 'axios'

/**
 * getUser function
 * creates a new user if api_token mismatch
 */
export function getUser () {
  // console.log('api_token', LocalStorage.get.item('api_token'))
  // if (LocalStorage.has('api_token')) {
  return axios({
    method: 'get',
    url: process.env.API + '/api/users/me',
    responseType: 'json',
    withCredentials: false,
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.get.item('api_token'),
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  })
  // } else {
  //   return false
  // }
}

/**
 * createUser function
 * creates a new user
 */
export function createUser () {
  return axios({
    method: 'post',
    url: process.env.API + '/api/users',
    responseType: 'json'
  })
}

/**
 * getSeconds function
 * gets server seconds
 */
export function getSeconds () {
  return axios({
    method: 'get',
    url: process.env.API + '/api/users/seconds',
    responseType: 'json'
  })
}
