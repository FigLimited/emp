import axios from 'axios'

export default ({ Vue }) => {
  Vue.prototype.$axios = axios.create({
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    withCredentials: false,
    baseURL: process.env.API + '/api/'
  })
}
