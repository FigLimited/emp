<?php

use Illuminate\Http\Request;
use App\Traits\TimeTrait;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:api'])->name('api.')->namespace('Api')->group(function () {
    Route::get('users/me/', 'UsersController@me')->name('users.me');
    Route::resource('users', 'UsersController');
    Route::post('resources/{resource}/make-idle/{pivot}', 'ResourcesController@makeIdle');
    Route::post('resources/{resource}/make-busy/{pivot}', 'ResourcesController@makeBusy');
    Route::post('resources/{resource}/buy', 'ResourcesController@buy');
    Route::post('resources/{resource}/sell', 'ResourcesController@sell');

    Route::post('processes/{process}/make-idle/{pivot}', 'ProcessesController@makeIdle');
    Route::post('processes/{process}/make-busy/{pivot}', 'ProcessesController@makeBusy');
    Route::post('processes/{process}/buy', 'ProcessesController@buy');
});

Route::middleware(['api'])->name('api.')->namespace('Api')->group(function () {
    Route::get('users/seconds', 'UsersController@seconds')->name('users.seconds');
    Route::post('users', 'UsersController@store')->name('users.store');
});
