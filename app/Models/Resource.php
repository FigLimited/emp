<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = [
        'stocks.quantity'
    ];

    /**
     * users relationship
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'resources_users')->withPivot(['id', 'level', 'active'])->using(ResourceUser::class);
    }

    /**
     * processes relationship
     */
    public function processes()
    {
        return $this->belongsToMany(Process::class, 'processes_resources')->withPivot('qty');
    }

    /**
     * process relationship
     */
    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    /**
     * stocks relationship
     */
    public function stocks()
    {
        return $this->belongsToMany(User::class, 'stocks');
    }

}
