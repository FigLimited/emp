<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'api_token', 'money'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'level'
    ];


    /**
     * getLevelAttribute accessor
     */
    public function getLevelAttribute()
    {
        return (is_null($this->xp)) ? 1 : intval(floor(log($this->xp, 2)) + 1);
    }

    /**
     * busy_resources relationship
     */
    public function busy_resources()
    {
        return $this->belongsToMany(Resource::class, 'resources_users')->where('busy', 1)->withPivot(['id', 'level'])->using(ResourceUser::class);
    }

    /**
     * idle_resources relationship
     */
    public function idle_resources()
    {
        return $this->belongsToMany(Resource::class, 'resources_users')->where('busy', 0)->withPivot(['id', 'level'])->using(ResourceUser::class);
    }

    /**
     * stocks relationship
     */
    public function stocks()
    {
        return $this->belongsToMany(Resource::class, 'stocks')->withPivot(['id', 'quantity'])->using(Stock::class);
    }

    /**
     * busy_processes relationship
     */
    public function busy_processes()
    {
        return $this->belongsToMany(Process::class, 'processes_users')->where('busy', 1)->withPivot(['id', 'level'])->using(ProcessUser::class);
    }

    /**
     * idle_processes relationship
     */
    public function idle_processes()
    {
        return $this->belongsToMany(Process::class, 'processes_users')->where('busy', 0)->withPivot(['id', 'level'])->using(ProcessUser::class);
    }

    /**
     * register method
     */
    public static function register($password)
    {
        $client = new Client();
        $result = $client->request('GET', 'https://randomuser.me/api/?inc=login');
        $json = json_decode((string)$result->getBody());
        $loginData = $json->results[0];

        //  random details to get started
        $user = self::create([
            'username' => $loginData->login->username,
            'password' => bcrypt($password),
            'xp' => 1,
            'money' => 100,
            'api_token' => $loginData->login->uuid
        ]);
        // $user->refresh();
        $user->load(['busy_resources', 'idle_resources', 'busy_processes', 'idle_processes']);

        $user->busy_resources()->attach(Resource::where('name', 'Hydrogen')->first()->id);
        $user->refresh();

        return $user;
    }
}
