<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $fillable = ['name', 'image', 'live'];

    /**
     * resources relationship
     */
    public function resources()
    {
        return $this->belongsToMany(Resource::class, 'processes_resources')->withPivot('qty');
    }
}

