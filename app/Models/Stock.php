<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Stock extends Pivot
{
    /**
     * set the database table name
     */
    protected $table = 'stocks';

    /**
     * avoid trying to update the timestamps
     */
    public $timestamps = false;

    protected $fillable = ['quantity'];
}
