<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\DB;

class ProcessUser extends Pivot
{
    /**
     * set the database table name
     */
    protected $table = 'processes_users';

    /**
     * avoid trying to update the timestamps
     */
    public $timestamps = false;

    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function timerUpdate()
    {
        $this->load('process.resources');
        $this->load('user.stocks');

        $stocks = $this->user->stocks->mapToGroups(function($item, $key) {
            return [$item['id'] => $item['pivot']['quantity']];
        })->toArray();

        $resources = $this->process->resources->mapToGroups(function($item, $key) {
            return [$item['id'] => $item['pivot']['qty']];
        })
        ->toArray()
        ;

        $allAvailableIsHere = true;
        foreach($resources as $key => $resource) {
            if($resource[0] < 0) {
                // check we have stock
                if(!isset($stocks[$key])) {
                    $allAvailableIsHere = false;
                } else if($stocks[$key][0] < abs($resource[0])) {
                    $allAvailableIsHere = false;
                }
            }
        }

        if($allAvailableIsHere) {
            foreach($resources as $resourceId => $resourceQuantity) {
                // check if resource exists
                if($this->user->stocks()->where('resource_id', $resourceId)->exists()) {
                    $existingQuantity = $this->user->stocks()->where('resource_id', $resourceId)->first();
                    $this->user->stocks()->updateExistingPivot($resourceId, ['quantity' => ($existingQuantity->pivot->quantity + $resourceQuantity[0])]);
                } else {
                    $this->user->stocks()->save(Resource::find($resourceId), ['quantity' => $resourceQuantity[0]]);
                }
            }
        }
    }
}
