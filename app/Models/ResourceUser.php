<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\User;

class ResourceUser extends Pivot
{
    /**
     * set the database table name
     */
    protected $table = 'resources_users';

    /**
     * avoid trying to update the timestamps
     */
    public $timestamps = false;

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }

    public function timerUpdate()
    {
        $stock = User::where('id', $this->user_id)->first()->stocks()->where('resource_id', $this->resource_id)->first();

        // error_log();
        if($stock == null) {
            //  create new record
            User::where('id', $this->user_id)->first()->stocks()->attach($this->resource_id, [
                'quantity' => $this->level
            ]);
        } else {
            //  update existing record
            User::where('id', $this->user_id)->first()->stocks()->updateExistingPivot($this->resource_id, ['quantity' => ($stock->pivot->quantity + $this->level)]);
        }

        //  add xp to user
        return $this->level;

    }
}
