<?php
namespace App\Traits;

trait TimeTrait
{
    public function getSeconds()
    {
        $t = microtime();
        $time = explode(" ", $t);
        $seconds = ($time[1] + round($time[0], 3)) % 30;
        return $seconds;
    }
}
