<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use App\Models\ProcessUser;
use App\Models\ResourceUser;
use App\Models\User;

trait UpdateTrait
{
    public function minuteUpdate()
    {
        $startTime = explode(" ", microtime());
        echo 'START: '.date("H:i:s", $startTime[1]).' '.$startTime[0].PHP_EOL;

        //  set all userXp to 0. Gets added to by script as appropriate
        $userXp = [];

        //  get all ResourceUser that are mineable
        $resourceUsers = ResourceUser::where('busy', 1)->get();

        foreach($resourceUsers as $resourceUser) {
            $xp = $resourceUser->timerUpdate();
            try {
                $userXp[$resourceUser->user_id] += $xp;
            } catch(\Exception $e) {
                $userXp[$resourceUser->user_id] = $xp;
            }
        }

        //  get all ProcessUser that are processible
        $processUsers = ProcessUser::where('busy', 1)->get();
        foreach($processUsers as $processUser) {
            $processUser->timerUpdate();
        }

        foreach($userXp as $k => $v) {
            User::where('id', $k)->update(['xp' => DB::raw('xp + '.$v)]);
        }

        $endTime = explode(" ", microtime());
        echo 'END:   '.date("H:i:s", $endTime[1]).' '.$endTime[0].PHP_EOL;

    }
}
