<?php

namespace App\Http\Controllers\Api;

use App\Models\Resource;
use App\Models\ResourceUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\TimeTrait;

class ResourcesController extends Controller
{
    use TimeTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $resource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $resource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        //
    }

    /**
     * make a pivot data entry idle
     */
    public function makeIdle(Request $request, Resource $resource, $pivotId)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        if($pivot = ResourceUser::find($pivotId)->update(['busy' => 0])) {
            return \Response::json([
                'pivot' => $pivot,
                'seconds' => $this->getSeconds()
            ], 200);
        }

        return \Response::json([], 412);
    }

    /**
     * make a pivot data entry busy
     */
    public function makeBusy(Request $request, Resource $resource, $pivotId)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        //  TODO: check if enough level
        if($user->level > count($user->busy_resources) && $pivot = ResourceUser::find($pivotId)->update(['busy' => 1])) {
            return \Response::json([
                'pivot' => $pivot,
                'seconds' => $this->getSeconds()
            ], 200);
        }

        return \Response::json([], 412);
    }

    /**
     * buy method
     */
    public function buy(Request $request, Resource $resource)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        //  got enough money?
        if($user->money >= $resource->cost_from_marketplace) {
            $user->idle_resources()->attach($resource->id, ['busy' => 0]);
            $user->money = $user->money - $resource->cost_from_marketplace;
            $user->save();
        }

        return \Response::json([
            'idle_resources' => $user->idle_resources,
            'money' => $user->money
            // 'seconds' => $this->getSeconds()
        ], 200);
    }

    /**
     * sell method
     */
    public function sell(Request $request, Resource $resource)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        $input = $request->all();

        //  reduce stock quantity
        $existing = $user->stocks()->where('resource_id', $resource->id)->first();
        // return response()->json([
        //     'existing' => $existing
        // ]);
        $user->stocks()->updateExistingPivot($resource->id, ['quantity' => $existing->pivot->quantity - $input['quantity']]);

        //  increase beer
        $user->money = $user->money + ($input['quantity'] * $resource->cost_to_marketplace);
        $user->save();

        $user->load('stocks');

        return response()->json([
            'input' => $input,
            'stocks' => $user->stocks,
            'money' => $user->money
        ]);
    }
}
