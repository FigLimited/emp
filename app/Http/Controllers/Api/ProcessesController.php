<?php

namespace App\Http\Controllers\Api;

use App\Models\Process;
use App\Models\ProcessUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\TimeTrait;

class ProcessesController extends Controller
{
    use TimeTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $process)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function edit(Process $process)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Process $process)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $process)
    {
        //
    }

    /**
     * make a pivot data entry idle
     */
    public function makeIdle(Request $request, Process $process, $pivotId)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        if($pivot = ProcessUser::find($pivotId)->update(['busy' => 0])) {
            return \Response::json([
                'pivot' => $pivot,
                'seconds' => $this->getSeconds()
            ], 200);
        }

        return response()->json([], 412);
    }

    /**
     * make a pivot data entry busy
     */
    public function makeBusy(Request $request, Process $process, $pivotId)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        //  TODO: check if enough level
        if($user->level > count($user->busy_processes) && $pivot = ProcessUser::find($pivotId)->update(['busy' => 1])) {
            return \Response::json([
                'pivot' => $pivot,
                'seconds' => $this->getSeconds()
            ], 200);
        }

        return \Response::json([], 412);
    }

    /**
     * buy method
     */
    public function buy(Request $request, Process $process)
    {
        //  TODO: do security checks
        $user = \App\Models\User::where('api_token', $request->bearerToken())->first();

        //  got enough money?
        if($user->money >= $process->cost_from_marketplace) {
            $user->idle_processes()->attach($process->id, ['busy' => 0]);
            $user->money = $user->money - $process->cost_from_marketplace;
            $user->save();
        }

        return \Response::json([
            'idle_processes' => $user->idle_processes,
            'money' => $user->money
            // 'seconds' => $this->getSeconds()
        ], 200);
    }
}
