<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Process;
use App\Models\Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\TimeTrait;

class UsersController extends Controller
{
    use TimeTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  random password
        $password = str_random(8);

        //  get a new User
        $user = User::register($password);

        $resources = Resource::where('live', 1)->where('is_extractable', 1)->orderBy('sort', 'ASC')->get();
        $processes = Process::where('live', 1)->orderBy('name', 'ASC')->get();

        return response()->json([
            'user' => $user,
            'resources' => $resources,
            'processes' => $processes,
            'password' => $password,
            'seconds' => $this->getSeconds()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->load(['busy_resources', 'idle_resources', 'stocks', 'busy_processes', 'idle_processes']);
        $resources = Resource::where('live', 1)->where('is_extractable', 1)->orderBy('sort', 'ASC')->get();
        $processes = Process::where('live', 1)->orderBy('name', 'ASC')->get();

        return response()->json([
            'user' => $user,
            'resources' => $resources,
            'processes' => $processes,
            'seconds' => $this->getSeconds()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * me method
     */
    public function me(Request $request)
    {
        $user = User::where('api_token', $request->bearerToken())->first();
        // return \Response::json(['user' => $user], 200);

        if($user !== null) {
            $user->load(['busy_resources', 'idle_resources', 'stocks', 'busy_processes', 'idle_processes']);
            $resources = Resource::where('live', 1)->where('is_extractable', 1)->orderBy('sort', 'ASC')->get();
            $processes = Process::where('live', 1)->orderBy('name', 'ASC')->get();

            return response()->json([
                'user' => $user,
                'resources' => $resources,
                'processes' => $processes,
                'seconds' => $this->getSeconds()
            ]);
        }

        return \Response::json(['user' => $user], 401);
    }

    /**
     * getSeconds method
     */
    public function seconds(Request $request)
    {
        return response()->json([
            'seconds' => $this->getSeconds()
        ]);
    }
}
